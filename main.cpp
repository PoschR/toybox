#include <iostream>
#include <array>

#include "include/dynamics.h"
#include "laboratory"


int main()
{
    toy::body_motion<toy::scalar> stone_motion(toy::mass(10));
    toy::dynamics& cast_dynamic = stone_motion;


    for(int i=0; i<20; i++)
    {
        stone_motion.add_force(10);
        cast_dynamic.evolve();

        std::cout << i * 0.1 << "s Current motion:\t" << stone_motion.current_velocity() << "m/s @ " << stone_motion.current_position() << "m" << std::endl;
    }


    return 0;
}