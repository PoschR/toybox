//
// Created by robert on 21.05.18.
//

#ifndef TOYBOX_DYNAMICS_H
#define TOYBOX_DYNAMICS_H

namespace toy
{
    using scalar = double;

    /**
     * Use this as a base interface for all systems that change with time.
     */
    class dynamics
    {
    public:
        virtual ~dynamics() = default;

        //Possible names: operate, enact, act, step, advance
        virtual void evolve() = 0;
    };

}


#endif //TOYBOX_DYNAMICS_H
